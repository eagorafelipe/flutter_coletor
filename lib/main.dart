import 'package:flutter/material.dart';
import 'package:flutter_coletor/UI/inicioUI.dart';

void main() => runApp(ColetorApp());

class ColetorApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Coletor Mobile",
      theme: ThemeData(
        primarySwatch: Colors.white,
        primaryColor: Colors.orangeAccent,
      ),
      home: InicioUI(),
    );
  }
}
